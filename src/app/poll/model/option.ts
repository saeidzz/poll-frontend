import {User} from "./user";

export class Option {
  id: number;
  content: string;
  percent: number;
  list: User[];

}
